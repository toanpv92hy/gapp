<?php
	session_start();
	include_once "examples/templates/base.php";
	require_once 'src/Google/autoload.php';
	$client_id = '531351299637-vuvbg18odrmlgqqt7cdfmrnrkgdbk9qi.apps.googleusercontent.com';
	 $client_secret = 'lXSVOu9Qa_cAppaVCbS4Ny46';
	 $redirect_uri = 'http://gapp.com/';

	/************************************************
	  Make an API request on behalf of a user. In
	  this case we need to have a valid OAuth 2.0
	  token for the user, so we need to send them
	  through a login flow. To do this we need some
	  information from our API console project.
	 ************************************************/
	$client = new Google_Client();
	$client->setClientId($client_id);
	$client->setClientSecret($client_secret);
	$client->setRedirectUri($redirect_uri);
	$client->addScope(
		array(
			"https://www.googleapis.com/auth/urlshortener",
			"https://www.googleapis.com/auth/plus.me",
			"https://www.googleapis.com/auth/plus.circles.read",
			"https://www.googleapis.com/auth/plus.circles.write",
			"https://www.googleapis.com/auth/plus.stream.read",
			"https://www.googleapis.com/auth/plus.stream.write",
			"https://www.googleapis.com/auth/plus.media.upload"
		)
		
	);
	/************************************************
	  When we create the service here, we pass the
	  client to it. The client then queries the service
	  for the required scopes, and uses that when
	  generating the authentication URL later.
	 ************************************************/
	$service = new Google_Service_Urlshortener($client);
	$plus = new Google_Service_Plus($client);
	$oauth2   = new Google_Service_Oauth2($client);
	

	/************************************************
	  If we're logging out we just need to clear our
	  local access token in this case
	 ************************************************/
	if (isset($_REQUEST['logout'])) {
	  unset($_SESSION['access_token']);
	}

	/************************************************
	  If we have a code back from the OAuth 2.0 flow,
	  we need to exchange that with the authenticate()
	  function. We store the resultant access token
	  bundle in the session, and redirect to ourself.
	 ************************************************/
	if (isset($_GET['code'])) {
	  $client->authenticate($_GET['code']);
	  $_SESSION['access_token'] = $client->getAccessToken();
	  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
	  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
	}

	/************************************************
	  If we have an access token, we can make
	  requests, else we generate an authentication URL.
	 ************************************************/
	if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	  $client->setAccessToken($_SESSION['access_token']);
	} else {
	  $authUrl = $client->createAuthUrl();
	}

	/************************************************
	  If we're signed in and have a request to shorten
	  a URL, then we create a new URL object, set the
	  unshortened URL, and call the 'insert' method on
	  the 'url' resource. Note that we re-store the
	  access_token bundle, just in case anything
	  changed during the request - the main thing that
	  might happen here is the access token itself is
	  refreshed if the application has offline access.
	 ************************************************/
	if ($client->getAccessToken() && isset($_GET['url'])) {
	  $url = new Google_Service_Urlshortener_Url();
	  $url->longUrl = $_GET['url'];
	  $short = $service->url->insert($url);
	  $_SESSION['access_token'] = $client->getAccessToken();
	}

	echo pageHeader("User Query - URL Shortener");
	if (strpos($client_id, "googleusercontent") == false) {
	  echo missingClientSecretsWarning();
	  exit;
	}
	?>
	<div class="box">
	<div class="request">
		<?php 
		if (isset($authUrl)) {
		  echo "<a class='login' href='" . $authUrl . "'>Connect Me!</a>";
		} else {
			var_dump($_SESSION);
	  	?>
	    <form id="url" method="GET" action="{$_SERVER['PHP_SELF']}">
	      <input name="url" class="url" type="text">
	      <input type="submit" value="Shorten">
	    </form>
	    <a class='logout' href='?logout'>Logout</a>
		<?php
		}
		?>
	</div>

	<div class="shortened">
		<?php
		if (isset($short)) {
		  var_dump($short);
		}
		?>
	</div>
	</div>
	<?php
	// echo pageFooter(__FILE__);
	
	 $plusdomains = new Google_Service_PlusDomains($client);
	 $activityObject = new Google_Service_PlusDomains_ActivityObject();
	 $activityObject->setOriginalContent("Test api adads");
	 $activityAccess = new Google_Service_PlusDomains_Acl();
	 $activityAccess->setDomainRestricted(true);
	 $resource = new Google_Service_PlusDomains_PlusDomainsAclentryResource();
	 $resource->setType("public");
	 $resources = array();      
	 $resources[] = $resource;
	 $activityAccess->setItems($resources);

	 $activity = new Google_Service_PlusDomains_Activity();
	 $activity->setObject($activityObject);
	 $activity->setAccess($activityAccess);
	 // var_dump($activityObject);die;
	 // $plusdomains->activities->insert("117060656368436448463", $activity);
	 $userProfile = $plus->people->get('me');
	 var_dump($userProfile);die;
	$fields = array(
		'object'=>array(
			'content'=>"13132132"
			),
		'access'=> array(
			"items"=>array(),
			"domainRestricted"=>true
		)
	);

	$post_field_string = http_build_query($fields, '', '&');
    $ch = curl_init('https://www.googleapis.com/plusDomains/v1/people/117060656368436448463/activities');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    	'Content-Type'=> 'application/json',
    	'Authorization'=> 'OAuth ya29.yAGQ2X-IMt1qScEcBBqcxeLJWr-p0CKJRLk4ebkbrFSryTWdGRKRca0GOfXnrsJDMIpveA')
    );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);
    $response = curl_exec($ch);
    curl_close($ch);
    // var_dump($response);
?>